#!/bin/bash

SSB_ROOM_DIR=$(grep DIR .env | cut -d '=' -f 2-)
mkdir $SSB_ROOM_DIR
chown -R 1000:1000 $SSB_ROOM_DIR
docker-compose up -d