# Coolab Docker Server

## Getting started

On a clean Ubuntu machine install Docker:
```bash
curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh
```

And also `docker-compose` ([original insctructions](https://docs.docker.com/compose/install/)):
```bash
sudo curl -L "https://github.com/docker/compose/releases/download/1.25.5/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

> Note: If the command docker-compose fails after installation, check your path. You can also create a symbolic link to /usr/bin or any other directory in your path.

```bash
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
```
## Setup the machine

- Create network: `docker network create nginx-proxy`
- [Create swap file](https://www.digitalocean.com/community/tutorials/how-to-add-swap-on-ubuntu-14-04) (modify with the swap size you want to create):
```
sudo fallocate -l 4G /swapfile
sudo chmod 600 /swapfile
sudo mkswap /swapfile
echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab
```

Do a `reboot` and make sure the swap was create correctly using `sudo swapon -s`.

## Let's start

Clone this repo to the `www` folder: `git clone https://gitlab.com/coolab1/coolab-play-server.git /var/www`. Edit the `whoami` service in the [nginx-proxy.yml](https://gitlab.com/coolab1/coolab-play-server/blob/master/nginx-proxy.yml) file with your own domain and e-mail.

Now you're finally ready to start the nginx proxy server. Jump to the directory with `cd /var/www` and run `docker-compose -f nginx-proxy.yml up -d`.

## Running a Wordpress site

Use one of the examples to create your own Wordpress:
```
mv coolab.org novo-wp.com
```

Inside the `novo-wp.com` folder `mv .env.example .env` and edit the necessary fields.

If you have a backup of existing Wordpress site move the `wp-content` folder of your old site inside `wp-app` and the sql dump file inside `wp-data`. That will start the project from the backup automatically.

Finally when everything is ready run `docker-compose up -d`. And voila!

Use `docker-compose down` to stop the services.

## Running Nextcloud

Navigate to the Nextcloud setup directory.

`cp .env.example .env`

Make sure to change the password: `vim .env`

`docker-compose up -d`


## Helpers

[tmate](https://github.com/tmate-io/tmate), [htop](https://htop.dev/) & [rsync](https://rsync.samba.org/)

```
sudo apt install tmate python3 htop rsync
```

 Install [z](https://github.com/rupa/z) for easy moving around:

 ```
git clone https://github.com/rupa/z/ ~/tmp/z
chmod +x ~/tmp/z/z.sh
mv ~/tmp/z/z.sh /usr/local/bin/
mv ~/tmp/z/z.1 /usr/local/share/man/man1
rm -rf ~/tmp/z
source /usr/local/bin/z.sh
echo '. /usr/local/bin/z.sh' >> ~/.bashrc
 ```

Use [docker-clean](https://github.com/ZZROTDesign/docker-clean) to occasionally cleanup Docker:
```
curl -s https://raw.githubusercontent.com/ZZROTDesign/docker-clean/v2.0.4/docker-clean |
sudo tee /usr/local/bin/docker-clean > /dev/null && \
sudo chmod +x /usr/local/bin/docker-clean
```

After setting it up just run `docker-clean` to cleanup.

Occasionally use [docker-bench-security](https://github.com/docker/docker-bench-security) to check how secure your docker is:
```
docker run --rm --net host --pid host --userns host --cap-add audit_control \
    -e DOCKER_CONTENT_TRUST=$DOCKER_CONTENT_TRUST \
    -v /etc:/etc:ro \
    -v /lib/systemd/system:/lib/systemd/system:ro \
    -v /usr/bin/containerd:/usr/bin/containerd:ro \
    -v /usr/bin/runc:/usr/bin/runc:ro \
    -v /usr/lib/systemd:/usr/lib/systemd:ro \
    -v /var/lib:/var/lib:ro \
    -v /var/run/docker.sock:/var/run/docker.sock:ro \
    --label docker_bench_security \
    docker/docker-bench-security
```
