# Install

Edit .env.example file and run:

`./run.sh`

To create admin user:

`docker-compose exec room sh`

`/app/cmd/insert-user/insert-user -repo /ssb-go-room-secrets @your-own-ssb-public-key`
